#!/bin/sh

JDK_RPM_FILE=jdk-7u80-linux-x64.rpm

rpm -i https://gerritforge.com/gerritforge-repo-1-2.noarch.rpm
yum -y install java-1.7.0-openjdk-devel
yum -y install git
yum -y install unzip
#yum -y localinstall /vagrant/files/$JDK_RPM_FILE
yum -y install gerrit

sudo ln -snf /etc/init.d/gerrit /etc/rc3.d/S90gerrit

iptables -I INPUT 5 -m state --state NEW -m tcp -p tcp --dport 8080 -j ACCEPT -m comment --comment "Gerrit Server port"
service iptables save
/etc/init.d/gerrit start
